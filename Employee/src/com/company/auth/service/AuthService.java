package com.company.auth.service;

import javax.jws.WebService;

import com.company.auth.bean.Employee;

@WebService(name = "AuthService", targetNamespace = "http://service.auth.company.com/")
public interface AuthService {
	public Employee getEmployee(String gid);

}