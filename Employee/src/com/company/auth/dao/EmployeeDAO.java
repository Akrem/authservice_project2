package com.company.auth.dao;

import com.company.auth.bean.Employee;

public class EmployeeDAO {
	private Employee[] _employeeRepository;

	public EmployeeDAO(){
		_employeeRepository = new Employee[] {
			CreateEmployee("0223938", "Sami", "Moha"),
			CreateEmployee("0223939", "Nahom", "Samsom"),
			CreateEmployee("0223940", "Dani", "Don")
		};
	}
	
	public Employee getEmployee(String gid) {
		for(int i=0; i < _employeeRepository.length;i++) {
			Employee emp = _employeeRepository[i];
			if (emp.getGid().equals(gid)) 
				return emp;
		}
		return null;
	}
	public Employee CreateEmployee(String id, String first, String last) {
		Employee emp = new Employee();
		emp.setGid(id);
		emp.setFirstName(first);
		emp.setLastName(last);
		return emp;
	}
}
